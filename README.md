# Cultured Foodies

Hong Kong's dim sum? Italy's lasagna? Or UK's fish 'n' chips? Join us on a journey of culinary and cultural discovery that stretches through the ages and across the seas. With information on delectable cuisines from around the world and where you can experience them in your city, as well as highlights on worldwide countries and U.S cities, Cultured Foodies makes it easy for you to feast to your mind and stomach's content.

## Group members

| Name              | EID     | Gitlab ID          |
| ----------------- | ------- | ------------------ |
| Joshua Arrojado   | jea2876 | @josharrojado      |
| Siddhesh Krishnan | sk46827 | @siddheshkrishnan1 |
| Lucinda Nguyen    | lon75   | @lucinda_nguyen    |
| Tim Nguyen        | tvn387  | @nguytim           |
| Amy Ouyang        | ao24395 | @amyouyang         |
| Vishal Tak        | vt5564  | @VishalTak14       |

## Git SHA

| Phase     | Git SHA                                  |
| --------- | ---------------------------------------- |
| Phase I   | 4bde46bdaf4329e224efc4b3a8db9f7eacb8aeed |
| Phase II  | 4fbf2dfd585fa3c90df9ae846b13d681cebb804c |
| Phase III | 067ca5f0e1b8ce2f25978eee9b6b45c6b62ac681 |
| Phase IV  | ddeef41996f44281c1a471f0a3a6146c8a5b388a |

## Project leader

Phase I: Lucinda Nguyen

Phase II: Siddhesh Krishnan

Phase III: Tim Nguyen

Phase IV: Amy Ouyang

## Link to GitLab pipelines

https://gitlab.com/cs373-group-11/cultured-foodies/-/pipelines

## Link to Postman API

https://documenter.getpostman.com/view/15165948/TzJrBeeW

## Link to website

https://www.culturedfoodies.me

## Link to Presentation

https://www.youtube.com/watch?v=L6nQpZWKnzU

## Completion times for each member

### Phase I

| Name              | Estimated Completion time | Actual Completion time |
| ----------------- | ------------------------- | ---------------------- |
| Joshua Arrojado   | 10 hours                  | 40 hours               |
| Siddhesh Krishnan | 25 hours                  | 35 hours               |
| Lucinda Nguyen    | 25 hours                  | 50 hours               |
| Tim Nguyen        | 25 hours                  | 50 hours               |
| Amy Ouyang        | 30 hours                  | 55 hours               |
| Vishal Tak        | 17 hours                  | 50 hours               |

### Phase II

| Name              | Estimated Completion time | Actual Completion time |
| ----------------- | ------------------------- | ---------------------- |
| Joshua Arrojado   | 30 hours                  | 30 hours               |
| Siddhesh Krishnan | 35 hours                  | 53 hours               |
| Lucinda Nguyen    | 35 hours                  | 55 hours               |
| Tim Nguyen        | 30 hours                  | 60 hours               |
| Amy Ouyang        | 35 hours                  | 60 hours               |
| Vishal Tak        | 35 hours                  | 50 hours               |

### Phase III

| Name              | Estimated Completion time | Actual Completion time |
| ----------------- | ------------------------- | ---------------------- |
| Joshua Arrojado   | 20 hours                  | 25 hours               |
| Siddhesh Krishnan | 20 hours                  | 25 hours               |
| Lucinda Nguyen    | 25 hours                  | 30 hours               |
| Tim Nguyen        | 15 hours                  | 16 hours               |
| Amy Ouyang        | 25 hours                  | 30 hours               |
| Vishal Tak        | 15 hours                  | 20 hours               |

### Phase IV

| Name              | Estimated Completion time | Actual Completion time |
| ----------------- | ------------------------- | ---------------------- |
| Joshua Arrojado   | 10 hours                  | 10 hours               |
| Siddhesh Krishnan | 10 hours                  | 10 hours               |
| Lucinda Nguyen    | 20 hours                  | 25 hours               |
| Tim Nguyen        | 15 hours                  | 20 hours               |
| Amy Ouyang        | 10 hours                  | 10 hours               |
| Vishal Tak        | 5 hours                   | 10 hours               |

## Comments
